#!/bin/bash

# Combines assignments into a combined file,
# to facilitate easier printing.

rm -f combined.md
touch combined.md

i=1

while [ -f assignments/$i.md ]; do
	cat assignments/$i.md >> combined.md
	echo -e '\n\n' >> combined.md
	let i=i+1
done
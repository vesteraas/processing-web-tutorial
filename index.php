<!DOCTYPE html>
<?php include 'getAssignment.php' ?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Processing - <?php echo $assignment['id']; ?>: <?php echo $assignment['title']; ?></title>
	<link href="http://netdna.bootstrapcdn.com/bootswatch/3.0.1/flatly/bootstrap.min.css" rel="stylesheet">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="style.css">
</head>
<body>

	<div class="container">

		<div id="oppgave">
			<h2><?php echo $assignment['id']; ?>: <?php echo $assignment['title']; ?></h2>
			<div id="text"><?php echo $assignment['text']; ?></div>
			<div id="nav">
				<a href="#" onclick="getPrevAssignment(); return false;" class="btn btn-default" id="prevButton"><i class="fa fa-arrow-left fa-4x"></i></a>
				<a href="#" onclick="getNextAssignment(); return false;" class="btn btn-default" id="nextButton"><i class="fa fa-arrow-right fa-4x"></i></a>
			</div>
		</div>

		<div id="left">
			<canvas id="sketch"></canvas>
		</div>

		<div id="right">
			<div id="editor"><?php echo $assignment['code']; ?></div>
			<p>
				<a href="#" onclick="playButton()" class="btn btn-success"><i class="fa fa-play"></i> Kjør</a>
				<a href="#" onclick="stopSketch()" class="btn btn-danger"><i class="fa fa-stop"></i> Stopp</a>
				<a href="#" onclick="resetCode()" class="btn btn-info"><i class="fa fa-code"></i> Nullstill kode</a>
				<a href="#help-modal" class="btn btn-info"><i class="fa fa-info"></i> Vis referanse</a>
			</p>
		</div>

		<p class="footer">
			<small><a href="https://bitbucket.org/vesteraas/processing-web-tutorial/">Åpen kildekode</a> | Laget av <a href="http://erik.vestera.as">Erik</a> | Feedback? Lyst til å hjelpe til? Send meg en e-post: <script type="text/javascript">
				String.prototype.rot13 = function(){
					return this.replace(/[a-zA-Z]/g, function(c){
						return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
					});
				};
				var s = '<n uers="znvygb:revx@irfgren.nf">revx@irfgren.nf</n>';
				document.write(s.rot13());
			</script></small>
		</p>

	</div>

	<?php include 'help-modal.php' ?>

	<script src="ace-builds/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js" type="text/javascript"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/processing.js/1.4.16/processing.min.js"></script>
	<script src="scripts.js" type="text/javascript"></script>
	<script>
		var assignment = <?php echo $assignment['id']; ?>;
		var hasNext = <?php echo $assignment['hasNext']; ?>

		var editor = ace.edit("editor");
		editor.setTheme("ace/theme/monokai");
		editor.getSession().setMode("ace/mode/java");

		// Temporary (hopefully) workaround for sketch not rendering on refresh
		setTimeout(function() {
			updateButtons();
			renderSketch();
		}, 10);

		$('a[href="#help-modal"]').click(function(event) {
			event.preventDefault();
			$(this).modal({
				zIndex: 5
			});
		});
	</script>
</body>
</html>

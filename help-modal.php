<div id="help-modal" style="display:none; width: 700px;">
	<p>Her er en referanse med de viktigste kommandoene. Vil du se alle kommandoene kan du se på <a href="http://processing.org/reference/">Processing sin egen referanse</a>.</p>

	<div style="width: 45%; float:left;">
		<h3>Tegning</h3>
		<p>Punkter er koordinater x, y hvor 0, 0 er øverst til venstre. x er altså hvor langt fra venstre side. y er hvor langt fra toppen.</p>
		<p><code>ellipse(x, y, bredde, høyde);</code></p>
		<p><code>rect(x, y, bredde, høyde);</code></p>
		<p><code>line(x1, y1, x2, y2);</code></p>
		<p><code>triangle(x1, y1, x2, y2, x3, y3);</code></p>
		<p><code>text("text" x, y);</code></p>

		<h3>Datatyper</h3>
		<p><code>int i = 1;</code></p>
		<p><code>float f = 2.5;</code></p>
		<p><code>String navn = "Bob";</code></p>
	</div>

	<div style="width: 45%; float:right;">
		<h3>Farger</h3>
		<p>Tallene i farge-kommandoene går fra 0 til 255.</p>
		<p><code>fill(r, g, b);</code></p>
		<p><code>fill(svart/hvitt);</code></p>
		<p><code>fill(r, g, b, gjennomsiktighet);</code></p>
		<p><code>background(r, g, b);</code></p>
		<p><code>background(svart/hvitt);</code></p>

		<h3>Variabler</h3>
		<p><code>width</code> og <code>height</code></p>
		<p><code>mouseX</code> og <code>mouseY</code></p>
	</div>

</div>
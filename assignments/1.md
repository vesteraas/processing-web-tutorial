## Velkommen!

Før vi setter i gang med selve kodingen er det greit at du blir komfortabel med denne løsningen.

Under denne teksten er to bokser. Til høyre ser du kode du kan redigere, og til venstre ser du resultatet av koden når den kjøres.

Denne første koden er bare en liten velkomst, og du trenger ikke tenke på hva den betyr. Men siden du sikkert ikke heter Bob kan du prøve å bytte ut `Bob` i koden med ditt eget navn.

Når du har gjort det kan du trykke på knappen hvor det står "Kjør". Når du har fått til dette kan du trykke på knappen øverst til høyre for å gå til neste oppgave. Koden blir lagret hver gang du trykker "Kjør", så du kan gå tilbake og se på det du har lært før om du trenger det.

---
~~~
background(0);
textAlign(CENTER);
textSize(35);

text("Hei Bob!", width / 2, height / 2);
~~~
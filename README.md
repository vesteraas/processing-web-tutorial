# Web-basert Tutorial for Processing

Som tittelen sier, en web-basert tutorial for [Processing](http://processing.org). Målgruppen for det nåværende oppgavesettet er folk som ikke har programmert før i det hele tatt.

[Live demo](http://vestera.as/processing/)

[Wiki](https://bitbucket.org/vesteraas/processing-web-tutorial/wiki/Home)

## Bruker
* [Processing.js](http://processingjs.org)
* [Ace Editor](http://ace.c9.io)
* [PHP Markdown](http://michelf.ca/projects/php-markdown/)
* [jQuery](http://jquery.com)
* [Twitter Bootstrap](http://getbootstrap.com/)
* [Font Awesome](http://fortawesome.github.io/Font-Awesome/)
* [jQuery Modal](https://github.com/kylefox/jquery-modal)

## Hvis du vil sette opp og teste

Så lenge du kan kjøre PHP 5.3+ bør dette være alt du trenger å gjøre:

~~~
git clone --recursive https://bitbucket.org/vesteraas/processing-web-tutorial.git
~~~

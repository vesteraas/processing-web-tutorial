<?php

# Install PSR-0-compatible class autoloader
spl_autoload_register(function($class){
	require 'php-markdown/'.preg_replace('{\\\\|_(?!.*\\\\)}', DIRECTORY_SEPARATOR, ltrim($class, '\\')).'.php';
});

# Get Markdown class
use \Michelf\MarkdownExtra;

if (isset($_GET['id'])) {
	$id = $_GET['id'];
	if ( ! is_numeric($id)) {
		exit("Invalid ID");
	}
} else {
	$id = 1;
}

if ( ! file_exists('assignments/'.$id.'.md')) {
	$id = 1;
}

$hasNext = (file_exists('assignments/'.($id + 1).'.md')) ? 1 : 0;

$assignment_file = file_get_contents('assignments/'.$id.'.md');

$split_assignment = explode("---", $assignment_file);
$title = trim(strtok($split_assignment[0], "\n"));
$title = str_replace("##", '', $title);
$text = trim(substr( $split_assignment[0], strpos($split_assignment[0], "\n")+1 ));
$text = MarkdownExtra::defaultTransform($text);
$code = $split_assignment[1];
$code = trim(str_replace("~~~", '', $code));

$assignment = array(
	'id' => $id,
	'title' => $title,
	'text' => $text,
	'code' => $code,
	'hasNext' => $hasNext
	);

if (__FILE__ == $_SERVER['DOCUMENT_ROOT'].$_SERVER['PHP_SELF']) {
	echo json_encode($assignment);
}

?>